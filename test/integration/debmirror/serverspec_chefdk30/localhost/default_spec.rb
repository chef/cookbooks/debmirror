#
# Copyright 2021 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <c.huhn@gsi.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

require 'spec_helper'

describe file '/etc/debmirror.d/cvmfs.sh' do
  it { should exist }
end

describe command 'bash -n /etc/debmirror.d/cvmfs.sh' do
  its(:exit_status) { should be_zero }
  its(:stdout) { should be_empty }
  its(:stderr) { should be_empty }
 end

describe command 'gpg --no-default-keyring --list-keys '\
                 '--keyring /srv/debmirror/.gnupg/trustedkeys.gpg' do
  its(:exit_status) { should be_zero }
  its(:stdout) { should contain 'CernVM Administrator' }
  # its(:stderr) { should be_empty } # complains about "unsafe ownership on /home/vagrant/.gnupg"
end

describe command 'sudo -u nobody LC_ALL=C /etc/debmirror.d/cvmfs.sh --dry-run' do
  its(:exit_status) { should be_zero }
  its(:stdout) { should be_empty }
  its(:stderr) { should be_empty }
end

describe command 'sudo -u nobody /etc/debmirror.d/cvmfs.sh --verbose --dry-run' do
  its(:exit_status) { should be_zero }
  its(:stdout) { should contain 'gpgv: Good signature from "CernVM Administrator' }
  its(:stdout) { should contain 'All done.' }
end

describe file '/srv/debmirror/cvmfs' do
  it { should exist }
  it { should be_directory }
end

describe command 'crontab -u nobody -l' do
  its(:exit_status) { should be_zero }
  its(:stdout) { should match %r{^((\d+|\*) ){5}run-parts --report --regex='\.sh\$' /etc/debmirror\.d$} }
  its(:stderr) { should be_empty }
end
