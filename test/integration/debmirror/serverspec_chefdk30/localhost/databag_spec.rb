#
# Copyright 2021 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <c.huhn@gsi.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

require 'spec_helper'

context 'x2go' do
  @repo_name = 'x2go'

  let(:key_id) { 'X2go Debian/Ubuntu Packaging' }

  describe file "/etc/debmirror.d/#{@repo_name}.sh" do
    it { should exist }
  end

  describe command 'gpg --no-default-keyring --list-keys '\
                   '--keyring /srv/debmirror/.gnupg/trustedkeys.gpg' do
    its(:exit_status) { should be_zero }
    its(:stdout) { should contain key_id }
  end

  describe command "sudo -u nobody LC_ALL=C /etc/debmirror.d/#{@repo_name}.sh"\
                   ' --dry-run' do
    its(:exit_status) { should be_zero }
    its(:stdout) { should be_empty }
    its(:stderr) { should be_empty }
  end

  describe command "sudo -u nobody LC_ALL=C /etc/debmirror.d/#{@repo_name}.sh"\
                   ' --verbose --dry-run' do
    its(:exit_status) { should be_zero }
    its(:stdout) { should match(/gpgv: Good signature from ".*#{key_id}/) }
    its(:stdout) { should contain 'All done.' }
  end

  describe file "/srv/debmirror/#{@repo_name}" do
    it { should exist }
    it { should be_directory }
  end
end
