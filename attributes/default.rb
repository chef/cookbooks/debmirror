# default architecture to mirror:
default_unless['debmirror']['architectures'] = %w[amd64]

# default basedir for mirrored repositories
default_unless['debmirror']['base_dir'] = '/srv/debmirror'

# components of the repo to be mirrored, default: "main"
default_unless['debmirror']['components'] = %w[main]

# do not use data bags by default
default_unless['debmirror']['data_bag'] = nil

# keyring where debmirror looks for repository keys
default_unless['debmirror']['keyring'] =
  node['debmirror']['base_dir'] + '/.gnupg/trustedkeys.gpg'

# the user that runs the scripts:
default_unless['debmirror']['mirrors'] = {}

# proxy for mirroring
default_unless['debmirror']['proxy'] = nil

# the directory the generated debmirror scripts are placed into:
default_unless['debmirror']['script_dir'] = '/etc/debmirror.d'

# the user that runs the scripts:
default_unless['debmirror']['user'] = 'nobody'

default_unless['debmirror']['cron'] = {
  day: '*',
  hour: 2,
  minute: 42
}
