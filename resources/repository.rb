#
# Copyright 2013-2022 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <c.huhn@gsi.de>
#  Dennis Klein       <d.klein@gsi.de>
#  Victor Penso       <v.penso@gsi.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

property :arch,         Array,  default: node['debmirror']['architectures']
property :components,   Array,  default: node['debmirror']['components']
property :deb_src,      [true, false], default: false
property :distribution, Array,  default: [node['lsb']['codename']]
property :user,         String, default: node['debmirror']['user']
property :key,          [String, nil]
property :keyring,      [String, nil], default: node['debmirror']['keyring']
property :server,       String, default: 'deb.debian.org'
property :proto,        String, default: 'http'
property :path,         String, default: '/'
property :proxy,        [String, nil], default: node['debmirror']['proxy']
property :mirror_dir,   [String, nil]
property :options,      Array,  default: %w[--getcontents]
property :script_dir,   String, default: node['debmirror']['script_dir']
property :rsync_extra,  [Array, String, nil],
         # turn strings into an array, default to ['trace']
         coerce: proc { |x|
                   if x.is_a?(String)
                     Array[x]
                   else
                     x.nil? ? %w[trace] : x
                   end
                 }
property :rsync_proxy,  [String, nil]

default_action :add

action :add do
  # TODO: use ruby-gpgme for key management
  if new_resource.key

    package 'gnupg'

    home = node['debmirror']['base_dir']
    keyring = new_resource.keyring

    directory ::File.dirname(keyring) do
      owner new_resource.user
    end

    # fingerprint = `gpg --with-colons --with-fingerprint <<<"#{new_resource.key}" | grep ...`

    # TODO: avoid re-runs
    execute "Adding repository key for #{new_resource.name}" do
      command "gpg --no-default-keyring --keyring #{keyring}" \
              " --import <<-EOD\n#{new_resource.key}\nEOD"
      user user
      # without $HOME gpg tries to create /root/.gnupg :(
      environment('HOME' => home)
      # not_if { `gpg --no-default-keyring --keyring #{keyring} --with-colons --fingerprint`match %r{^fpr:+#{fingerpring}:$} }
    end
  end

  storage = new_resource.mirror_dir ||
            "#{node['debmirror']['base_dir']}/#{new_resource.name}"

  # Make sure the archive directory exists
  directory storage do
    owner new_resource.user
    recursive true
  end

  # Generate the mirror script
  template "#{new_resource.script_dir}/#{new_resource.name}.sh" do
    source 'debmirror.sh.erb'
    mode '0755'
    variables(
      release: new_resource.distribution,
      arch:    new_resource.arch,
      section: new_resource.components,
      server:  new_resource.server,
      proto:   new_resource.proto,
      path:    new_resource.path,
      proxy:   new_resource.proxy,
      storage: storage,
      keyring: new_resource.keyring,
      options: new_resource.options,
      rsync_extra: new_resource.rsync_extra,
      rsync_proxy: new_resource.rsync_proxy
    )
  end
end

action :remove do
  file "#{script_dir}/#{name}.sh" do
    action :delete
  end

  # TODO: Remove key from keyring?
end
