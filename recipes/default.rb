# Cookbook Name:: debmirror
# Recipe:: default
#
# Copyright 2013-2023 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn   <C.Huhn@gsi.de>
#  Dennis Klein       <d.klein@gsi.de>
#  Victor Penso       <v.penso@gsi.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

package 'debmirror'

# Holds the script to sync with package archives
directory node['debmirror']['script_dir']

# Holds the mirrored repositories:
directory node['debmirror']['base_dir']

# Configure all mirrors defined by attributes
node['debmirror']['mirrors'].each do |name, conf|
  databag = conf['databag'] || node['debmirror']['data_bag']

  begin
    if data_bag(databag).include?(name)
      conf = conf.merge(data_bag_item(databag, name))
    end
  rescue Net::HTTPServerException => e
    # ignore 404 "Not Found"
    next unless e.response.code == '404'
  end

  # use the global default proxy unless defined otherwise:
  proxy = conf['proxy'] || node['debmirror']['proxy']

  # debmirror may require a proxy for rsync
  #  this is not read from the --proxy option
  #  but has to be defined as an env var
  #  without the protocol prefix required for the proxy option
  rsync_proxy = conf['rsync_proxy']
  rsync_proxy ||= proxy.gsub(%r{^\w+://},'') if proxy

  debmirror_repository name do
    server       conf['server']
    proto        conf['method'] if conf['method']
    path         conf['path']
    distribution conf['release'] if conf['release']
    components   conf['section'] if conf['section']
    deb_src      conf['deb_src'] if conf['deb_src']
    arch         conf['arch']    if conf['arch']
    key          conf['key']
    proxy        proxy
    rsync_extra  conf['rsync_extra']
    rsync_proxy  rsync_proxy
    options      conf['options']
  end
end

# FIXME: cron_d instead
cron 'debmirror_update' do
  user   node['debmirror']['user']
  minute node['debmirror']['cron']['minute']
  hour   node['debmirror']['cron']['hour']
  day    node['debmirror']['cron']['day']
  mailto node['debmirror']['notify'] if node['debmirror']['notify']
  home   node['debmirror']['base_dir']
  # we cannot prevent this error message:
  #  therefore we don't `run-parts --report`
  command "run-parts --report --regex='\.sh$' #{node['debmirror']['script_dir']}"
end
