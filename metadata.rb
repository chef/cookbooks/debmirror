name             'debmirror'
maintainer       'GSI Helmholtzzentrum fuer Schwerionenforschung GmbH'
maintainer_email 'linuxgroup@gsi.de'
license          'GNU Public License 3.0'
description      'Installs/Configures debmirror'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.4.0'

source_url       'https://git.gsi.de/chef/cookbooks/debmirror'
issues_url       'https://git.gsi.de/chef/cookbooks/debmirror/issues'
