# debmirror CHANGELOG

This file is used to list changes made in each version of the debmirror cookbook.

## 0.4.0 - 2024-05-24

### Added
- add option to define a rsync proxy,
  derive it from the HTTP proxy unless explictly defined for a repo

### Changed

- use a global proxy default unless one has been explictly defined for a repo
- Add `--report` to run_parts invocation so mirror script names end up in cron emails

## 0.3.0 - 2023-xx-xx

### Added
- make default architecture globally configurable via node attribute
- make default architecture globally configurable via node attribute

### Changed
- modernize test suite

## 0.2.1 - 2021-06-18

### Added
- configure mirroring via proxy
